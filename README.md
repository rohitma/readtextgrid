# README #

### What is this repository for? ###
To read the labels of an annotated file saved in .TextGrid format, created using Praat. 
Returns a python list of dicts. Each dict has the minimum and maximum timestamps, the duration and the labels. 

The code, as it is, reads the following labels: notes and melodic atoms.

Created by

Ashwin Kalyan,
Rohit Ananthanarayana
(Music and Audio Research Group, NITK)